#include <stdlib.h>
#include "../arrayLib.h"
int main(int argc, char const *argv[]){
  array vec,vec1,vec2,vec3;

  printf("\n Cargar el vector vec[]:\n");
  // Cargar con funcion load()
  char mensaje[] = "  vec[%d] = "; // Mensaje/descripcion para iterar (%d es la posicion)
  // char mensaje[] = "  vec[%d/%d] = "; // en este caso imprimiria la posicion y el total de posiciones

  int limite = 5; // Cant de elementos

  load(&vec, mensaje, limite);
  //load(&vec, (char*) "  vec[%d] = ", 5);
  ///(*) No require inicializar

  // Mostrar vector
  printf("\n Vector autocarga: ");
  printArray(vec);

  //cargar desde otros arreglos
  int arr[] = {1,2,3,4,5,6};
  float arr1[] = {1.1,2.2,3.3,4.4,5.5};
  double arr2[] = {1.1,2.2,3.3,4.4,5.5};
  //Requiere inicializar
  ini(vec1); ini(vec2); ini(vec3);
  //Cargar
  //vec1 apartir de un arrego de enteros
  insertInt(&vec1,arr,lenght(arr)); // lenght(x) devuelve la dimension del arreglo
  //vec2 apartir de un arreglo de floats
  insertFloat(&vec2,arr1,lenght(arr1));
  //vec3 apartir de un arreglo de doubles
  insert(&vec3,arr2,lenght(arr2));
  //Mostrar
  printf("\n (int) vec1 : ");
  printArray(vec1);
  printf("\n (float) vec2 : ");
  printArray(vec2);
  printf("\n (double) vec3 : ");
  printArray(vec3);
  system("pause");
  return 0;
}
