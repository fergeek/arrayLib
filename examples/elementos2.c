#include <stdlib.h>
#include "../arrayLib.h"
void arrayPrinter(array vec);
int main(int argc, char const *argv[]){
  array vec;
  printf("\n Cargar el vector vec[]:\n");
  // Cargar el vector
  load(&vec, (char*) "  vec[%d] = ", 9);
  //Imprimir el vector
  printf("\n vec = ");
  arrayPrinter(vec);
  //Primer elemento
  printf(" Primer elemento: %d\n", (int) firstEle(vec));
  //Primer elemento
  printf(" Ultimo elemento: %d\n", (int) lastEle(vec));
  //Ordenar el array
  sort(&vec); // ordenar de mayor a menor
  printf(" Vector ordenado : ");
  arrayPrinter(vec);
  //Invertir el orden
  invert(&vec);
  printf(" Vector invertido : ");
  arrayPrinter(vec);
  //Intercambiar posiciones
  switchEle(&vec,0,8); // intercambia el primer elemento por el ultimo
  printf(" Posiciones intercambiadas : ");
  arrayPrinter(vec);
  //Eliminar un elemento
  deleteEle(&vec,0); // elimina el elemento 0
  system("pause");
  return 0;
}
void arrayPrinter(array vec){
  printArrAvcd(vec,(char*)"%.lf",(char*)", ",(char*)"[",(char*)"]\n");
}
