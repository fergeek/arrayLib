#include <stdlib.h>
#include "../arrayLib.h"
int main(int argc, char const *argv[]){
  array vec;
  printf("\n Cargar el vector vec[]:\n");
  // Cargar el vector
  load(&vec, "  vec[%d] = ", 5);
  //Imprimir el vector
  printf("\n Vector: ");
  printArray(vec); // default -> \n {x1, x2,..., xn}\n (xi = E,DD)
  // configurable
  printf("\n Vector (vista configurada): ");
  char form[] = "%.lf"; // formato
  char del[]  = ","; // delimitador
  char pre[] = "[";  // prefijo
  char suf[] = "]\n";  // sufijo
  printArrAvcd(vec, form, del, pre, suf);
  // Equivale a: printArrAvcd(vec,"%.lf",",","[","]");
  system("pause");
  return 0;
}
