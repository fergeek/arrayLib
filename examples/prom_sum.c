#include <stdlib.h>
#include "../arrayLib.h"
void arrayPrinter(array vec);
int main(int argc, char const *argv[]){
  array vec;
  printf("\n Cargar el vector vec[]:\n");
  // Cargar el vector
  load(&vec, (char*) "  vec[%d] = ", 5);
  arrayPrinter(vec);
  //Sumatoria sum()
  printf(" Sumatoria: %.2f\n",(float) sum(vec));
  //Maximo
  printf(" Maximo: %d\n",(int) max(vec));
  //Minimo
  printf(" Minimo: %d\n",(int) min(vec));
  //Primedio mean()
  printf(" Promedio: %.2f\n",(float) mean(vec));
  system("pause");
  return 0;
}
void arrayPrinter(array vec){
  printArrAvcd(vec,(char*)"%.lf",(char*)", ",(char*)"[",(char*)"]\n");
}
