#include <stdio.h>
#define max_size 900
#define lenght(x)  (int) (sizeof(x) / sizeof((x)[0]))
#define ini(X) X.lenght = 0
typedef struct {
  double content[max_size];
  int lenght;
} array;
void load(array *vec,char msj[], int limit);
void push(array *vec, double element);
void sort(array *vec);
void deleteEle(array *vec, int pos);
void switchEle(array *vec, int Ele1, int Ele2);
void invert(array *vec);
void reWrite(array *vec, int p, double e);
void printArray(array vec);
void printArrAvcd(array vec, char format[],char del[], char pre[], char suf[]);
void errorDisplay(char func[]);
void unshift(array *vec, double Ele);
array merge(array vec1, array vec2);
array reverse(array vec);
array search(array vec,double search);
int indexOf(array vec, double search);
double firstEle(array vec);
double lastEle(array vec);
double pop(array *vec);
double shift(array *vec);
double sum(array vec);
double mean(array vec);
double val(array vec, int p);
double max(array vec);
double min(array vec);
double prodEsc(array vec1, array vec2);
void insertInt(array *vec, int arr[],int size);
void insertFloat(array *vec, float arr[],int size);
void insert(array *vec, double arr[],int size);
//// //// //// //// //// //// //// //// //// //// //// ////
void load(array *vec,char msj[], int limit){
  if(limit <= max_size){
    vec -> lenght = 0;
    for(int i = 0; i < limit; i++){
      printf(msj, i, limit-1);
      scanf("%lf", &vec -> content[i]);
      vec -> lenght+=1;
    }
  }
  else {
    errorDisplay((char*) "load");
  }
}
void printArray(array vec){
  printArrAvcd(vec,(char*) "%.2lf", (char*) ", ", (char*) "\n {", (char*) "}\n");
}
void printArrAvcd(array vec, char format[],char del[], char pre[], char suf[]){
  printf("%s",pre);
  for(int i = 0; i < vec.lenght; i++){
    printf(format, vec.content[i]);
    if(i != vec.lenght-1){printf("%s",del);}
  }
  printf("%s",suf);
}
void push(array *vec, double element){
  vec -> content[(vec -> lenght)] = element;
  vec -> lenght++;
}
void reWrite(array *vec, int p, double e){
  if(p < vec -> lenght){
    vec -> content[p] = e;
  }
  else{
    errorDisplay((char*) "reWrite");
  }
}
void deleteEle(array *vec, int pos){
  int i;
  if(pos <= (vec -> lenght)){
    for(i = pos; i <= (vec -> lenght) - 1; i++){
      switchEle(vec,i,i+1);
    }
      vec -> lenght--;
  }
}
void switchEle(array *vec, int Ele1, int Ele2){
  double aux;
  if((Ele1 <= max_size && Ele2 <= max_size) && (Ele1 <= (vec -> lenght) && Ele2 <= (vec -> lenght))){
    aux = vec -> content[Ele1];
    vec -> content[Ele1] = vec -> content[Ele2];
    vec -> content[Ele2] = aux;
  }
  else{
    errorDisplay((char*) "switchEle");
  }
}
void sort(array *vec){
  int i,c = 0;
  while(c != (vec -> lenght - 1)){
    c = 0;
    for(i = 1; i < (vec -> lenght) ; i++){
      if((vec -> content[i]) > (vec -> content[i-1])){switchEle(&*vec,i,i-1);}
      else{c++;}
    }
  }
}
void invert(array *vec){
  array ret = reverse(*vec);
  *vec = ret;
}
void errorDisplay(char func[]){
  printf("\n array -> %s() : Param error...\n",func);
}
void unshift(array *vec, double Ele){
  array aux;
  aux.content[0] = Ele;
  aux.lenght = 1;
  *vec = merge(aux,*vec);
}
void insertInt(array *vec, int arr[],int size){
  int i;
  for(i = 0; i < size; i++){
    vec -> content[(vec -> lenght)+i] = arr[i];
  }
  vec -> lenght += i;
}
void insertFloat(array *vec, float arr[],int size){
  int i;
  for(i = 0; i < size; i++){
    vec -> content[(vec -> lenght)+i] = arr[i];
  }
  vec -> lenght += i;
}
void insert(array *vec, double arr[],int size){
  int i;
  for(i = 0; i < size; i++){
    vec -> content[(vec -> lenght)+i] = arr[i];
  }
  vec -> lenght += i;
}
array merge(array vec1, array vec2){
  array ret; int i, lenght = vec1.lenght + vec2.lenght;
  for(i = 0; i < lenght; i++){
    if(i<vec1.lenght){
      ret.content[i] = vec1.content[i];
    }
    else{
      ret.content[i] = vec2.content[i-vec1.lenght];
    }
  }
  ret.lenght = lenght;
  return ret;
}
array reverse(array vec){
  int i, c = 0; array ret;
  ret.lenght = vec.lenght;
  for(i = vec.lenght - 1; i >= 0; i--){
    ret.content[c] = vec.content[i];
    c++;
  }
  return ret;
}
array search(array vec,double search){
  int i; array ret; ret.lenght = 0;
  for(i = 0; i < vec.lenght; i++){
    if(search == vec.content[i]){
      push(&ret,(double) i);
    }
  }
  return ret;
}
int indexOf(array vec, double search){
  int i,p = -1;
  for(i = 0; i < vec.lenght; i++){
    if(search == vec.content[i]){p = i;break;}
  }
  return p;
}
double sum(array vec){
  int i; double ac = 0;
  for(i = 0; i < vec.lenght; i++){ac += vec.content[i];}
  return ac;
}
double firstEle(array vec){
  return vec.content[0];
}
double lastEle(array vec){
  return vec.content[vec.lenght - 1];
}
double pop(array *vec){
  double aux = vec -> content[(vec -> lenght - 1)];
  vec -> content[(vec -> lenght - 1)] = 0;
  vec -> lenght--;
  return aux;
}
double shift(array *vec){
  double aux = vec -> content[0];
  deleteEle(*&vec,0);
  return aux;
}
double mean(array vec){
  return sum(vec)/vec.lenght;
}
double val(array vec, int p){
  if(p < max_size){
    return vec.content[p];
  }
  else{return -1;}
}
double prodEsc(array vec1, array vec2){
  int i; double ac = 0;
  for(i = 0; i < vec1.lenght; i++){ac += vec1.content[i] * vec2.content[i];}
  return ac;
}
double max(array vec){
  int i, m = vec.content[0];
  for(i=0;i<vec.lenght;i++){
    if(vec.content[i]>m){m = vec.content[i];}
  } return m;
}
double min(array vec){
  int i, m = vec.content[0];
  for(i=0;i<vec.lenght;i++){
    if(vec.content[i]<m){m = vec.content[i];}
  } return m;
}
